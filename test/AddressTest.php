<?php

namespace Faker\Test\Vietnam;

use Faker\Generator;
use Faker\Vietnam\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    public function setUp(): void
    {
        $faker = new Generator();
        $faker->seed(1);
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    public function testPostCode()
    {
        $pattern = '/^\d{6}$|^\d{5}$|^\d{5}-\d{4}$/';

        $postCode = $this->_faker->postCode;
        $this->assertMatchesRegularExpression($pattern, $postCode);
    }

    public function testCity()
    {
        $city = $this->_faker->city;
        $this->assertSame(true, is_string($city) && $city !== '', 'City name is not a valid string');
    }

    public function testProvince()
    {
        $province = $this->_faker->province;
        $this->assertSame(true, is_string($province) && $province !== '', 'Province name is not a valid string');
    }
}
