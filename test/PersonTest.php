<?php

namespace Faker\Test\Vietnam;

use Exception;
use Faker\Generator;
use Faker\Vietnam\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    public function setUp(): void
    {
        $faker = new Generator();
        $faker->seed(1);
        $faker->addProvider(new Person($faker));
        $this->_faker = $faker;
    }

    public function testPersonalId()
    {
        $personalId = $this->_faker->personalIdentityNumber;
        $this->assertEquals(12, strlen($personalId));
    }

    public function testPersonalIdGender()
    {
        $personalId = $this->_faker->personalIdentityNumber(Person::GENDER_MALE);
        $this->assertEquals(0, substr($personalId, 3, 1) % 2);
    }

    public function testPersonalIdDateOfBirth()
    {
        $personalId = $this->_faker->personalIdentityNumber(null, new \DateTime('10-11-2023'));
        $centuryNumber = substr($personalId, 3, 1);
        $this->assertEquals(2, $centuryNumber % 2 == 0 ? $centuryNumber : $centuryNumber - 1);
        $this->assertEquals('23', substr($personalId, 4, 2));
    }

    public function testPersonalIdDateOfBirthMale()
    {
        $personalId = $this->_faker->personalIdentityNumber(Person::GENDER_MALE, new \DateTime('10-11-2023'));
        $this->assertEquals(2, substr($personalId, 3, 1));
        $this->assertEquals('23', substr($personalId, 4, 2));
    }

    public function testPersonalIdDateOfBirthFemale()
    {
        $personalId = $this->_faker->personalIdentityNumber(Person::GENDER_FEMALE, new \DateTime('10-11-2023'));
        $this->assertEquals(3, substr($personalId, 3, 1));
        $this->assertEquals('23', substr($personalId, 4, 2));
    }

    public function testPersonalIdBirthPlace()
    {
        $personalId = $this->_faker->personalIdentityNumber(null, null, '200');
        $this->assertEquals('200', substr($personalId, 0, 3));
    }

    public function testPersonalIdBirthPlaceNumeric()
    {
        $personalId = $this->_faker->personalIdentityNumber(null, null, 200);
        $this->assertEquals(200, substr($personalId, 0, 3));
    }

    public function testPersonalIdBirthPlaceSmallNumeric()
    {
        $personalId = $this->_faker->personalIdentityNumber(null, null, 2);
        $this->assertEquals('002', substr($personalId, 0, 3));
    }

    public function testPersonalIdBirthPlaceTooLong()
    {
        $this->expectException(Exception::class);
        $this->expectErrorMessage('regionNumber must consist of three numbers');
        $this->_faker->personalIdentityNumber(null, null, '2000');
    }

    public function testPersonalIdBirthPlaceArray()
    {
        $this->expectException(Exception::class);
        $this->expectErrorMessage('regionNumber must a string of numbers or integer');
        $this->_faker->personalIdentityNumber(null, null, ['123']);
    }

    public function testPersonalIdBirthPlaceNonNumeric()
    {
        $this->expectException(Exception::class);
        $this->expectErrorMessage('regionNumber must a string of numbers or integer');
        $this->_faker->personalIdentityNumber(null, null, 'abc');
    }

    public function testPersonalIdBirthPlaceFloatNumber()
    {
        $this->expectException(Exception::class);
        $this->expectErrorMessage('regionNumber must consist of three numbers');
        $this->_faker->personalIdentityNumber(null, null, '3.1');
    }
}
