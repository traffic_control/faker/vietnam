<?php

namespace Faker\Vietnam;

use Faker\Extension\Extension;

class Internet extends \Faker\Provider\Internet implements Extension
{
    protected static $tld = ['com', 'com', 'com', 'com', 'com', 'com', 'biz', 'info', 'net', 'org', 'vn', 'com.vn', 'biz.vn', 'edu.vn',
        'gov.vn', 'net.vn', 'org.vn', 'int.vn', 'ac.vn', 'pro.vn', 'info.vn', 'health.vn', 'name.vn', 'mil.vn'
    ];
}
